// Fill out your copyright notice in the Description page of Project Settings.


#include "STEngineSubsystem.h"
#include "AbilitySystemGlobals.h"


void USTEngineSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	UAbilitySystemGlobals::Get().InitGlobalData();
}
