// Fill out your copyright notice in the Description page of Project Settings.


#include "ST_PlayerStateAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "Net/UnrealNetwork.h"
#include "SurvivalTaiga/Core/Game/Characters/ST_CharacterBase.h"

UST_PlayerStateAttributeSet::UST_PlayerStateAttributeSet()
{
}

void UST_PlayerStateAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, HealthRegenRate, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, Hungry, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, MaxHungry, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, HungryIncreaseRate, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, Cold, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, MaxCold, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, ColdIncreaseRate, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, Armor, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, MaxArmor, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, MoveSpeed, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, CharacterLevel, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UST_PlayerStateAttributeSet, XP, COND_None, REPNOTIFY_Always);
}

void UST_PlayerStateAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	// If a Max value changes, adjust current to keep Current % of Current to Max
	if (Attribute == GetMaxHealthAttribute()) // GetMaxHealthAttribute comes from the Macros defined at the top of the header
		{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
		}
	else if (Attribute == GetMaxHungryAttribute())
	{
		AdjustAttributeForMaxChange(Hungry, MaxHungry, NewValue, GetHungryAttribute());
	}
	else if (Attribute == GetMaxColdAttribute())
	{
		AdjustAttributeForMaxChange(Cold, MaxCold, NewValue, GetColdAttribute());
	}
	else if (Attribute == GetMaxArmorAttribute())
	{
		AdjustAttributeForMaxChange(Armor, MaxArmor, NewValue, GetArmorAttribute());
	}
	else if (Attribute == GetMoveSpeedAttribute())
	{
		// Cannot slow less than 150 units/s and cannot boost more than 1000 units/s
		NewValue = FMath::Clamp<float>(NewValue, 150, 1000);
	}
}

void UST_PlayerStateAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
    Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();
	FGameplayTagContainer SpecAssetTags;
	Data.EffectSpec.GetAllAssetTags(SpecAssetTags);

	AActor* TargetActor = nullptr;
	AController* TargetController = nullptr;
	AST_CharacterBase* TargetCharacter = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		TargetCharacter = Cast<AST_CharacterBase>(TargetActor);
	}

	// Get the Source actor
	AActor* SourceActor = nullptr;
	AController* SourceController = nullptr;
	AST_CharacterBase* SourceCharacter = nullptr;
	if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
	{
		SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
		SourceController = Source->AbilityActorInfo->PlayerController.Get();
		if (SourceController == nullptr && SourceActor != nullptr)
		{
			if (APawn* Pawn = Cast<APawn>(SourceActor))
			{
				SourceController = Pawn->GetController();
			}
		}

		if (SourceController)
		{
			SourceCharacter = Cast<AST_CharacterBase>(SourceController->GetPawn());
		}
		else
		{
			SourceCharacter = Cast<AST_CharacterBase>(SourceActor);
		}

		if (Context.GetEffectCauser())
		{
			SourceActor = Context.GetEffectCauser();
		}
	}
	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));
	} 
	else if (Data.EvaluatedData.Attribute == GetColdAttribute())
	{
		SetCold(FMath::Clamp(GetCold(), 0.0f, GetMaxCold()));
	}
	else if (Data.EvaluatedData.Attribute == GetHungryAttribute())
	{
		SetHungry(FMath::Clamp(GetHungry(), 0.0f, GetMaxHungry()));
	}
}

void UST_PlayerStateAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
                                                              const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
	{
		// Change current value to maintain the current Val / Max percent
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}

void UST_PlayerStateAttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, Health, OldHealth);
}

void UST_PlayerStateAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, MaxHealth, OldMaxHealth);
}

void UST_PlayerStateAttributeSet::OnRep_HealthRegenRate(const FGameplayAttributeData& OldHealthRegenRate)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, HealthRegenRate, OldHealthRegenRate);
}

void UST_PlayerStateAttributeSet::OnRep_Hungry(const FGameplayAttributeData& OldHungry)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, Hungry, OldHungry);
}

void UST_PlayerStateAttributeSet::OnRep_MaxHungry(const FGameplayAttributeData& OldMaxHungry)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, MaxHungry, OldMaxHungry);
}

void UST_PlayerStateAttributeSet::OnRep_HungryIncreaseRate(const FGameplayAttributeData& OldHungryIncreaseRate)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, HungryIncreaseRate, OldHungryIncreaseRate);
}

void UST_PlayerStateAttributeSet::OnRep_Cold(const FGameplayAttributeData& OldCold)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, Cold, OldCold);
}

void UST_PlayerStateAttributeSet::OnRep_MaxCold(const FGameplayAttributeData& OldMaxCold)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, MaxCold, OldMaxCold);
}

void UST_PlayerStateAttributeSet::OnRep_ColdIncreaseRate(const FGameplayAttributeData& OldColdIncreaseRate)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, ColdIncreaseRate, OldColdIncreaseRate);
}

void UST_PlayerStateAttributeSet::OnRep_Armor(const FGameplayAttributeData& OldArmor)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, Armor, OldArmor);
}

void UST_PlayerStateAttributeSet::OnRep_MaxArmor(const FGameplayAttributeData& OldMaxArmor)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, MaxArmor, OldMaxArmor);
}

void UST_PlayerStateAttributeSet::OnRep_MoveSpeed(const FGameplayAttributeData& OldMoveSpeed)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, MoveSpeed, OldMoveSpeed);
}

void UST_PlayerStateAttributeSet::OnRep_CharacterLevel(const FGameplayAttributeData& OldCharacterLevel)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, CharacterLevel, OldCharacterLevel);
}

void UST_PlayerStateAttributeSet::OnRep_XP(const FGameplayAttributeData& OldXP)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UST_PlayerStateAttributeSet, XP, OldXP);
}

