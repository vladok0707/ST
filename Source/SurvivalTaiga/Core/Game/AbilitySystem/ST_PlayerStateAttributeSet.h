// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "ST_PlayerStateAttributeSet.generated.h"


#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)
/**
 * 
 */
UCLASS()
class SURVIVALTAIGA_API UST_PlayerStateAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:

	UST_PlayerStateAttributeSet();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	
	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_Health)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, Health)

	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_MaxHealth)
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, MaxHealth)

	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_HealthRegenRate)
	FGameplayAttributeData HealthRegenRate;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, HealthRegenRate)

	UPROPERTY(BlueprintReadOnly, Category = "Hungry", ReplicatedUsing = OnRep_Hungry)
	FGameplayAttributeData Hungry;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, Hungry)

	UPROPERTY(BlueprintReadOnly, Category = "Hungry", ReplicatedUsing = OnRep_MaxHungry)
	FGameplayAttributeData MaxHungry; 
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, MaxHungry)

	UPROPERTY(BlueprintReadOnly, Category = "Hungry", ReplicatedUsing = OnRep_HungryIncreaseRate)
	FGameplayAttributeData HungryIncreaseRate; 
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, HungryIncreaseRate)

	UPROPERTY(BlueprintReadOnly, Category = "Cold", ReplicatedUsing = OnRep_Cold)
	FGameplayAttributeData Cold;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, Cold)

	UPROPERTY(BlueprintReadOnly, Category = "Cold", ReplicatedUsing = OnRep_MaxCold)
	FGameplayAttributeData MaxCold;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, MaxCold)

	UPROPERTY(BlueprintReadOnly, Category = "Cold", ReplicatedUsing = OnRep_ColdIncreaseRate)
	FGameplayAttributeData ColdIncreaseRate;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, ColdIncreaseRate)

	UPROPERTY(BlueprintReadOnly, Category = "Armor", ReplicatedUsing = OnRep_Armor)
	FGameplayAttributeData Armor;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, Armor)

	UPROPERTY(BlueprintReadOnly, Category = "Armor", ReplicatedUsing = OnRep_MaxArmor)
	FGameplayAttributeData MaxArmor;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, MaxArmor)

	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, Damage)

	UPROPERTY(BlueprintReadOnly, Category = "MoveSpeed", ReplicatedUsing = OnRep_MoveSpeed)
	FGameplayAttributeData MoveSpeed;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, MoveSpeed)

	UPROPERTY(BlueprintReadOnly, Category = "CharacterLevel", ReplicatedUsing = OnRep_CharacterLevel)
	FGameplayAttributeData CharacterLevel; 
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, CharacterLevel)

	UPROPERTY(BlueprintReadOnly, Category = "XP", ReplicatedUsing = OnRep_XP)
	FGameplayAttributeData XP;
	ATTRIBUTE_ACCESSORS(UST_PlayerStateAttributeSet, XP)
	
protected:

	// Helper function to proportionally adjust the value of an attribute when it's associated max attribute changes.
	// (i.e. When MaxHealth increases, Health increases by an amount that maintains the same percentage as before)
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);

	
	UFUNCTION()
	virtual void OnRep_Health(const FGameplayAttributeData& OldHealth);

	UFUNCTION()
	virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth);

	UFUNCTION()
	virtual void OnRep_HealthRegenRate(const FGameplayAttributeData& OldHealthRegenRate);

	UFUNCTION()
	virtual void OnRep_Hungry(const FGameplayAttributeData& OldHungry);

	UFUNCTION()
	virtual void OnRep_MaxHungry(const FGameplayAttributeData& OldMaxHungry);

	UFUNCTION()
	virtual void OnRep_HungryIncreaseRate(const FGameplayAttributeData& OldHungryIncreaseRate);

	UFUNCTION()
	virtual void OnRep_Cold(const FGameplayAttributeData& OldCold);

	UFUNCTION()
	virtual void OnRep_MaxCold(const FGameplayAttributeData& OldMaxCold);

	UFUNCTION()
	virtual void OnRep_ColdIncreaseRate(const FGameplayAttributeData& OldColdIncreaseRate);

	UFUNCTION()
	virtual void OnRep_Armor(const FGameplayAttributeData& OldArmor);

	UFUNCTION()
	virtual void OnRep_MaxArmor(const FGameplayAttributeData& OldMaxArmor);

	UFUNCTION()
	virtual void OnRep_MoveSpeed(const FGameplayAttributeData& OldMoveSpeed);

	UFUNCTION()
	virtual void OnRep_CharacterLevel(const FGameplayAttributeData& OldCharacterLevel);

	UFUNCTION()
	virtual void OnRep_XP(const FGameplayAttributeData& OldXP);

};
