// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "SurvivalTaiga/Defenitions/ST_Enums.h"

#include "ST_BaseGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALTAIGA_API UST_BaseGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UST_BaseGameplayAbility();


	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	ESTAbilityInputID AbilityInputID = ESTAbilityInputID::None;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	ESTAbilityInputID AbilityID = ESTAbilityInputID::None;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
	bool ActivateAbilityOnGranted = false;

	virtual void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
};
