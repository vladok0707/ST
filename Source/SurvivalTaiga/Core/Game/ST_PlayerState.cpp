// Fill out your copyright notice in the Description page of Project Settings.


#include "ST_PlayerState.h"

#include "Characters/ST_CharacterBase.h"

AST_PlayerState::AST_PlayerState()
{

	AbilitySystemComponent = CreateDefaultSubobject<UST_AbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	AttributeSetBase = CreateDefaultSubobject<UST_PlayerStateAttributeSet>(TEXT("AttributeSetBase"));

	NetUpdateFrequency = 100.0f;

	// Cache tags
	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
	FrozenTag = FGameplayTag::RequestGameplayTag(FName("State.Frozen"));
}

UST_PlayerStateAttributeSet* AST_PlayerState::GetAttributeSetBase() const
{
	return AttributeSetBase;
}

bool AST_PlayerState::IsAlive() const
{
	return GetHealth() > 0.0f;
}

bool AST_PlayerState::IsFrozen() const
{
	return GetCold() >= GetMaxCold();
}

float AST_PlayerState::GetHealth() const
{
	return AttributeSetBase->GetHealth();
}

float AST_PlayerState::GetMaxHealth() const
{
	return AttributeSetBase->GetMaxHealth();
}

float AST_PlayerState::GetHealthRegenRate() const
{
	return AttributeSetBase->GetHealthRegenRate();
}

float AST_PlayerState::GetHungry() const
{
	return AttributeSetBase->GetHungry();
}

float AST_PlayerState::GetMaxHungry() const
{
	return AttributeSetBase->GetMaxHungry();
}

float AST_PlayerState::GetHungryIncreaseRate() const
{
	return AttributeSetBase->GetHungryIncreaseRate();
}

float AST_PlayerState::GetCold() const
{
	return AttributeSetBase->GetCold();
}

float AST_PlayerState::GetMaxCold() const
{
	return AttributeSetBase->GetMaxCold();
}

float AST_PlayerState::GetColdIncreaseRate() const
{
	return AttributeSetBase->GetColdIncreaseRate();
}

float AST_PlayerState::GetArmor() const
{
	return AttributeSetBase->GetArmor();
}

float AST_PlayerState::GetMaxArmor() const
{
	return AttributeSetBase->GetMaxArmor();
}

float AST_PlayerState::GetMoveSpeed() const
{
	return AttributeSetBase->GetMoveSpeed();
}

int32 AST_PlayerState::GetXP() const
{
	return AttributeSetBase->GetXP();
}

void AST_PlayerState::BeginPlay()
{
	if (AbilitySystemComponent)
	{
		// Attribute change callbacks
		HealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).AddUObject(this, &AST_PlayerState::HealthChanged);
		MaxHealthChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxHealthAttribute()).AddUObject(this, &AST_PlayerState::MaxHealthChanged);
		HealthRegenRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthRegenRateAttribute()).AddUObject(this, &AST_PlayerState::HealthRegenRateChanged);
		HungryChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHungryAttribute()).AddUObject(this, &AST_PlayerState::HungryChanged);
		MaxHungryChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxHungryAttribute()).AddUObject(this, &AST_PlayerState::MaxHungryChanged);
		HungryIncreaseRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHungryIncreaseRateAttribute()).AddUObject(this, &AST_PlayerState::HungryIncreaseRateChanged);
		ColdChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetColdAttribute()).AddUObject(this, &AST_PlayerState::ColdChanged);
		MaxColdChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxColdAttribute()).AddUObject(this, &AST_PlayerState::MaxColdChanged);
		ColdIncreaseRateChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetColdIncreaseRateAttribute()).AddUObject(this, &AST_PlayerState::ColdIncreaseRateChanged);
		ArmorChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetArmorAttribute()).AddUObject(this, &AST_PlayerState::ArmorChanged);
		MaxArmorChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetMaxArmorAttribute()).AddUObject(this, &AST_PlayerState::MaxArmorChanged);
		CharacterLevelChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetCharacterLevelAttribute()).AddUObject(this, &AST_PlayerState::CharacterLevelChanged);
		XPChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetXPAttribute()).AddUObject(this, &AST_PlayerState::XPChanged);
	}
}

void AST_PlayerState::HealthChanged(const FOnAttributeChangeData& Data)
{
	AST_CharacterBase* Hero = Cast<AST_CharacterBase>(GetPawn());
	if (!IsAlive() && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
	{
		if (Hero)
		{
			Hero->Die();
		}
	}
}

void AST_PlayerState:: MaxHealthChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::HealthRegenRateChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::HungryChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::MaxHungryChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::HungryIncreaseRateChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::ColdChanged(const FOnAttributeChangeData& Data)
{
	AST_CharacterBase* Hero = Cast<AST_CharacterBase>(GetPawn());
	if (IsFrozen() && !AbilitySystemComponent->HasMatchingGameplayTag(FrozenTag))
	{
		if (Hero)
		{
			Hero->Frozen(true);
		}
	}
	if (!IsFrozen() && AbilitySystemComponent->HasMatchingGameplayTag(FrozenTag))
	{
		if (Hero)
		{
			Hero->Frozen(false);
		}
	}
}

void AST_PlayerState::MaxColdChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::ColdIncreaseRateChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::ArmorChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::MaxArmorChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::CharacterLevelChanged(const FOnAttributeChangeData& Data)
{
	
}

void AST_PlayerState::XPChanged(const FOnAttributeChangeData& Data)
{
	
}

