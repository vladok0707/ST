// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystem/ST_AbilitySystemComponent.h"
#include "AbilitySystem/ST_PlayerStateAttributeSet.h"

#include "ST_PlayerState.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALTAIGA_API AST_PlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AST_PlayerState();

	FORCEINLINE virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override
	{
		return AbilitySystemComponent;
	}

	class UST_PlayerStateAttributeSet* GetAttributeSetBase() const;

	UFUNCTION(BlueprintCallable)
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable)
	bool IsFrozen() const;

	//getters

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHealthRegenRate() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHungry() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMaxHungry() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHungryIncreaseRate() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetCold() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMaxCold() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetColdIncreaseRate() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetArmor() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMaxArmor() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMoveSpeed() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	int32 GetXP() const;
	
protected:
	
	UPROPERTY(VisibleAnywhere, Category = "Gameplay")
	UST_AbilitySystemComponent* AbilitySystemComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Gameplay")
	UST_PlayerStateAttributeSet* AttributeSetBase = nullptr;
	
	FGameplayTag DeadTag;

	FGameplayTag FrozenTag;

	FDelegateHandle HealthChangedDelegateHandle;
	FDelegateHandle MaxHealthChangedDelegateHandle;
	FDelegateHandle HealthRegenRateChangedDelegateHandle;

	FDelegateHandle HungryChangedDelegateHandle;
	FDelegateHandle MaxHungryChangedDelegateHandle;
	FDelegateHandle HungryIncreaseRateChangedDelegateHandle;

	FDelegateHandle ColdChangedDelegateHandle;
	FDelegateHandle MaxColdChangedDelegateHandle;
	FDelegateHandle ColdIncreaseRateChangedDelegateHandle;

	FDelegateHandle ArmorChangedDelegateHandle;
	FDelegateHandle MaxArmorChangedDelegateHandle;

	FDelegateHandle CharacterLevelChangedDelegateHandle;
	FDelegateHandle XPChangedDelegateHandle;

	virtual void BeginPlay() override;
	
	// Attribute changed callbacks
	virtual void HealthChanged(const FOnAttributeChangeData& Data);
	virtual void MaxHealthChanged(const FOnAttributeChangeData& Data);
	virtual void HealthRegenRateChanged(const FOnAttributeChangeData& Data);
	
	virtual void HungryChanged(const FOnAttributeChangeData& Data);
	virtual void MaxHungryChanged(const FOnAttributeChangeData& Data);
	virtual void HungryIncreaseRateChanged(const FOnAttributeChangeData& Data);

	virtual void ColdChanged(const FOnAttributeChangeData& Data);
	virtual void MaxColdChanged(const FOnAttributeChangeData& Data);
	virtual void ColdIncreaseRateChanged(const FOnAttributeChangeData& Data);

	virtual void ArmorChanged(const FOnAttributeChangeData& Data);
	virtual void MaxArmorChanged(const FOnAttributeChangeData& Data);

	virtual void CharacterLevelChanged(const FOnAttributeChangeData& Data);
	virtual void XPChanged(const FOnAttributeChangeData& Data);
};
