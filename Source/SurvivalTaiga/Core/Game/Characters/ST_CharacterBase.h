// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "GameFramework/Character.h"
#include "SurvivalTaiga/Core/Game/AbilitySystem/ST_AbilitySystemComponent.h"
#include "SurvivalTaiga/Core/Game/AbilitySystem/ST_PlayerStateAttributeSet.h"
#include "SurvivalTaiga/Defenitions/ST_Enums.h"

#include "ST_CharacterBase.generated.h"

class AST_CharacterBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterDiedDelegate, AST_CharacterBase*, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCharacterFrozenDelegate, AST_CharacterBase*, Character, bool, bStatus);

UCLASS()
class SURVIVALTAIGA_API AST_CharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:

	AST_CharacterBase();

	UPROPERTY(BlueprintAssignable)
	FCharacterDiedDelegate OnCharacterDied;

	UPROPERTY(BlueprintAssignable)
	FCharacterFrozenDelegate OnCharacterFrozen;

	UFUNCTION(BlueprintPure)
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual int32 GetAbilityLevel(ESTAbilityInputID AbilityID) const;

	// Removes all CharacterAbilities. Can only be called by the Server. Removing on the Server will remove from Client too.
	virtual void RemoveCharacterAbilities();


	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	int32 GetCharacterLevel() const;
	
	// Gets the Current value of MoveSpeed
	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMoveSpeed() const;

	// Gets the Base value of MoveSpeed
	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMoveSpeedBaseValue() const;

	virtual void Die();

	virtual void Frozen(bool bStatus);

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual void FinishDying();

protected:

	virtual void BeginPlay() override;

	UPROPERTY()
	UST_AbilitySystemComponent* AbilitySystemComponent;
	
	UPROPERTY()
	UST_PlayerStateAttributeSet* AttributeSetBase;


	FGameplayTag DeadTag;
	FGameplayTag EffectRemoveOnDeathTag;
	FGameplayTag FrozenTag;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UAnimMontage* DeathMontage;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Abilities")
	TArray<TSubclassOf<class UST_BaseGameplayAbility>> CharacterAbilities;


	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Abilities")
	TSubclassOf<class UGameplayEffect> DefaultAttributes;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Abilities")
	TArray<TSubclassOf<class UGameplayEffect>> StartupEffects;


	virtual void AddCharacterAbilities();

	virtual void InitializeAttributes();

	virtual void AddStartupEffects();


	/**
	* Setters for Attributes. Only use these in special cases like Respawning, otherwise use a GE to change Attributes.
	* These change the Attribute's Base Value.
	*/

	virtual void SetHealth(float Health);

};
