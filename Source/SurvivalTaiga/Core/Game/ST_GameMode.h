// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameMode.h"
#include "ST_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALTAIGA_API AST_GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AST_GameMode();


protected:

	virtual void BeginPlay() override;
	
};
