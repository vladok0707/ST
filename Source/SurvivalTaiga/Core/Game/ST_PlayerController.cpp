// Fill out your copyright notice in the Description page of Project Settings.


#include "ST_PlayerController.h"

#include "ST_PlayerState.h"

void AST_PlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AST_PlayerState* PS = GetPlayerState<AST_PlayerState>();
	if (PS)
	{
		// Init ASC with PS (Owner) and our new Pawn (AvatarActor)
		PS->GetAbilitySystemComponent()->InitAbilityActorInfo(PS, InPawn);
	}
}

void AST_PlayerController::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();
}
