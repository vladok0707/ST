// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ST_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALTAIGA_API AST_PlayerController : public APlayerController
{
	GENERATED_BODY()


protected:


	// Server only
	virtual void OnPossess(APawn* InPawn) override;

	virtual void OnRep_PlayerState() override;
};
