// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ST_Enums.generated.h"

UENUM(BlueprintType)
enum class ESTAbilityInputID : uint8
{
	// 0 None
	None			UMETA(DisplayName = "None"),
	// 1 Confirm
	Confirm			UMETA(DisplayName = "Confirm"),
	// 2 Cancel
	Cancel			UMETA(DisplayName = "Cancel"),
	// KEY - 1
	Ability1		UMETA(DisplayName = "Ability1"),
	// KEY - 1
	Ability2		UMETA(DisplayName = "Ability2"),
};